# kerberos-test-server

Set up a combined KDC / admin server for Kerberos testing inside GitLab Inc.

## MIT and Heimdal

There are two mainstream open source Kerberos implementations: MIT and
Heimdal. They are compatible for normal use, but the admin interface
`kadmin` is not compatible between the two.

The KDC and admin server at kerberos.gitlap.com use MIT Kerberos.
Therefore to use `kadmin` you need to be on a machine with MIT
Kerberos.

OS X comes with Heimdal Kerberos pre-installed. That means you can
configure your Mac to use kerberos.gitlap.com but `kadmin` will not
work.

```
# this is what it looks like when Heimdal kadmin fails to talk to MIT Kerberos
$ kadmin -p admin/gitlab
kadmin: kadm5_init_with_password: init_sec_context failed with 851968/-1765328189
```

## Client setup

Kerberos is time sensitive! The server at kerberos.gitlap.com should
sync to NTP. Make sure all client machines you use (both the GitLab
server and the end user machine) have their clocks in sync. On Ubuntu
this can be done with: `sudo ntpdate pool.ntp.org`.

### Debian / Ubuntu

```
sudo apt-get install krb5-user
```

- realm: KERBEROS.GITLAP.COM
- Kerberos server: kerberos.gitlap.com
- Admin server: kerberos.gitlap.com

Verify access with `kadmin -p gitlab/admin` (password in 1password).

### OS X

```
# Back up existing krb5.conf
sudo mv /etc/krb5.conf /etc/krb5.conf.$(date +%s)

# Configure OS X to use kerberos.gitlap.com
sudo tee /etc/krb5.conf <<EOF
[libdefaults]
	default_realm = KERBEROS.GITLAP.COM

[realms]
	KERBEROS.GITLAP.COM = {
		kdc = kerberos.gitlap.com
		admin_server = kerberos.gitlap.com
	}
EOF
```

Once you have created a user principal (see below) you can test your
setup with `kinit USERNAME`.

### Create a user principal

On an MIT Kerberos (e.g. Ubuntu) client machine:

```
kadmin -p gitlab/admin
# kadmin: prompt appears
addprinc janedoe
# enter+confirm password
quit
```

Save your user/password combination in your 'Personal' 1password vault
(the username is: `janedoe@KERBEROS.GITLAP.COM`).

Enable omniauth-kerberos on your GitLab server; also see
http://doc.gitlab.com/ee/integration/kerberos.html .

```
gitlab_rails['omniauth_enabled'] = true
gitlab_rails['omniauth_allow_single_sign_on'] = ['kerberos']
gitlab_rails['omniauth_providers'] = [
  {   
    "name" => "kerberos"
  }
]
```

Now if your GitLab server is configured as a Kerberos client (see
above), you can log into the web UI with `janedoe` and your password.

### Kerberized Git HTTP access

When you enable Kerberized Git HTTP access you can use your 'janedoe'
credentials for `git clone` etc. on a Kerberos client machine, without
having to transmit your user's Kerberos master password to the GitLab
server.

First add a service principal on the GitLab server. We assume the
GitLab server is already a Kerberos client. You need to specify the
FQDN by which GitLab users will reach the GitLab server.

On the GitLab server:

```
sudo kadmin -p gitlab/admin
# prompt appears
addprinc -randkey HTTP/<GITLAB SERVER FQDN>
ktadd -k /etc/gitlab.keytab HTTP/<GITLAB SERVER FQDN>
quit
```

You have now created a keytab file for GitLab: /etc/gitlab.keytab. The
'git' user needs access to this file.

```
sudo chown git:git /etc/gitlab.keytab
sudo chmod 0600 /etc/gitlab.keytab
```

Enable Kerberized Git HTTP access on your GitLab server: also see
http://doc.gitlab.com/ee/integration/kerberos.html#http-git-access .

```
gitlab_rails['kerberos_enabled'] = true
gitlab_rails['kerberos_keytab'] = "/etc/gitlab.keytab"
gitlab_rails['kerberos_use_dedicated_port'] = true
gitlab_rails['kerberos_port'] = 8443
```

After running reconfigure you should be able to use `git clone` on a Kerberos client machine.

```
kinit -p janedoe
# kinit asks for password
git clone http://:@<GITLAB SERVER FQDN>:8443/foo/bar.git
# clone happens
klist
# see ticket for HTTP/<GITLAB SERVER FQDN>
```

## Kerberos server setup: manual steps

These steps are for setting up **kerberos.gitlap.com**.

After running this cookbook for the first time on a new server you
need to do the following manual steps:

- generate a master database password (and store it in 1password)
- set the master database password with `sudo krb5_newrealm`
- generate a Kerberos admin account (store in 1password) with username `gitlab/admin`

```
sudo kadmin.local
# prompt appears: kadmin.local:
addprinc gitlab/admin
# enter + confirm password
quit
```

## Supported Platforms

Ubuntu 16.04.

## License and Authors

Author:: Jacob Vosmaer <jacob@gitlab.com>

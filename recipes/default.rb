package 'krb5-kdc'
package 'krb5-admin-server'
package 'haveged' # more entropy in /dev/random

directory '/etc/krb5kdc' do
  owner 'root'
  group 'root'
  mode '0700'
end

template '/etc/krb5kdc/kdc.conf'
template '/etc/krb5kdc/kadm5.acl'
template '/etc/krb5.conf'
